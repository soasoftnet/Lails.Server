﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Autofac;

namespace Lails.Server
{
    public class AppIDAttribute : ActionFilterAttribute
    {
        public AppIDAttribute()
        {

        }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);

            
            //var appid = actionContext.Request.Headers.ToDictionary(k => k.Key, v => v.Value)["api_key"];
            // AutofacConfig.Builder.Register(c => { return actionContext.Request.Headers.ToDictionary(k => k.Key, v => v.Value)["api_key"]; }).As<string>().InstancePerRequest();
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
        }
    }
}
